#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  
#  Copyright 2018 youcef sourani <youssef.m.sourani@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import dbus
from dbus.mainloop.glib import DBusGMainLoop
from gi.repository import GLib

DBusGMainLoop(set_as_default=True)

state_ids = {
             40 : "Connecting\n",
             50 : "Network Diconnect\n",
             60 : "Network Connect Without Internet\n",
             70 : "Internet Actived\n"
}
# NetworkManager State Code https://developer.gnome.org/NetworkManager/stable/nm-dbus-types.html#NMState


    

def notification(msg,app_name="",replaces_id=1,app_icon="",summary="",\
                timeout=5000,actions="",hints=""):
    bus = dbus.SessionBus()
    obj = bus.get_object("org.freedesktop.Notifications","/org/freedesktop/Notifications")
    interf = dbus.Interface(obj,"org.freedesktop.Notifications")  
    interf.Notify(app_name,replaces_id,app_icon,\
                summary,msg,actions,hints,timeout) 

    
def notify_state(state_id): # int
    if state_id in state_ids.keys():
        notification(msg=state_ids[state_id],summary="Testing {}".format(__file__))
        print(state_ids[state_id])


if __name__ == "__main__":
    bus = dbus.SystemBus()
    obj = bus.get_object("org.freedesktop.NetworkManager","/org/freedesktop/NetworkManager")
    interf = dbus.Interface(obj,"org.freedesktop.NetworkManager")
    interf.connect_to_signal("StateChanged", notify_state)

    loop = GLib.MainLoop()
    loop.run()










